/**
 * \file ConfigurationIsr.h
 * \brief Interrupts configuration.
 *
 *
 * \version iLLD_Demos_0_1_0_11
 * \copyright Copyright (c) 2014 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Demo_QspiDmaDemo_SrcDoc_InterruptConfig Interrupt configuration
 * \ingroup IfxLld_Demo_QspiDmaDemo_SrcDoc
 */

#ifndef CONFIGURATIONISR_H
#define CONFIGURATIONISR_H

#include "IfxSrc_cfg.h"

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/

/** \brief Build the ISR configuration object
 * \param no interrupt priority
 * \param cpu assign CPU number
 */
#define ISR_ASSIGN(no, cpu)  ((no << 8) + cpu)

/** \brief extract the priority out of the ISR object */
#define ISR_PRIORITY(no_cpu) (no_cpu >> 8)

/** \brief extract the service provider  out of the ISR object */
#define ISR_PROVIDER(no_cpu) (no_cpu % 8)
/**
 * \addtogroup IfxLld_Demo_QspiDmaDemo_SrcDoc_InterruptConfig
 * \{ */

/**
 * \name Interrupt priority configuration for CPU.
 * The interrupt priority range is [1,255]
 * \{
 */

#define ISR_PRIORITY_TIMER          1 /**< \brief Define the 1ms timer interrupt priority.  */

#define ISR_PRIORITY_ASCLIN0_TX     12   /**< \brief Define the ASCLIN1 transmit interrupt priority.  */
#define ISR_PRIORITY_ASCLIN0_RX     13   /**< \brief Define the ASCLIN1 receive interrupt priority.  */
#define ISR_PRIORITY_ASCLIN0_ER     14   /**< \brief Define the ASCLIN1 error interrupt priority.  */

#define ISR_PRIORITY_ASCLIN1_TX     15   /**< \brief Define the ASCLIN1 transmit interrupt priority.  */
#define ISR_PRIORITY_ASCLIN1_RX     16   /**< \brief Define the ASCLIN1 receive interrupt priority.  */
#define ISR_PRIORITY_ASCLIN1_ER     17   /**< \brief Define the ASCLIN1 error interrupt priority.  */

#define ISR_PRIORITY_ASCLIN3_TX     18   /**< \brief Define the ASCLIN1 transmit interrupt priority.  */
#define ISR_PRIORITY_ASCLIN3_RX     19   /**< \brief Define the ASCLIN1 receive interrupt priority.  */
#define ISR_PRIORITY_ASCLIN3_ER     20   /**< \brief Define the ASCLIN1 error interrupt priority.  */

#define ISR_PRIORITY_QSPI0_TX       21   /**< \brief Define the Qspi0 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI0_RX       22   /**< \brief Define the Qspi0 receive interrupt priority.  */
#define ISR_PRIORITY_QSPI0_ER       23 /**< \brief Define the Qspi0 error interrupt priority.  */

#define ISR_PRIORITY_QSPI1_TX       24   /**< \brief Define the Qspi1 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI1_RX       25   /**< \brief Define the Qspi1 receive interrupt priority.  */
#define ISR_PRIORITY_QSPI1_ER       26 /**< \brief Define the Qspi1 error interrupt priority.  */

#define ISR_PRIORITY_QSPI2_ER       27  /**< \brief Define the Qspi2 error interrupt priority.  */
#define ISR_PRIORITY_QSPI2_TX       28  /**< \brief Define the Qspi2 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI2_RX       29  /**< \brief Define the Qspi2 receive interrupt priority.  */

#define ISR_PRIORITY_QSPI3_ER       30  /**< \brief Define the Qspi2 error interrupt priority.  */
#define ISR_PRIORITY_QSPI3_TX       31  /**< \brief Define the Qspi2 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI3_RX       32  /**< \brief Define the Qspi2 receive interrupt priority.  */

#define ISR_PRIORITY_CAN_TX         33                           /* Define the CAN TX interrupt priority              */
#define ISR_PRIORITY_CAN_RX         34                           /* Define the CAN RX interrupt priority              */

#define ISR_PRIORITY_DMA_CH1        35 /**< \brief Define the Dma channel1 interrupt priority.  */               // CPU
#define ISR_PRIORITY_DMA_CH2        36 /**< \brief Define the Dma channel2 interrupt priority.  */               // CPU

#define INTERRUPT_ENCODER_GPT12 	37

#define ISR_PRIORITY_MODULE_GTM_TOM0            36
#define ISR_PRIORITY_MODULE_GTM_TOM1            37

/** \} */

#define INTERRUPT_VADC_RESULT_GROUP0 38
#define INTERRUPT_VADC_RESULT_GROUP1 39
#define INTERRUPT_VADC_RESULT_GROUP2 40
#define INTERRUPT_VADC_RESULT_GROUP3 41

#define STM0_ISR_PRIORITY 			65	/**< \brief Define the  timer interrupt priority. for Core 0  */


/**
 * \name DMA channel configuration.
 * The DMA channel range is [0,15] for TC22X/TC23X
 * \{
 */

#define TFT_DMA_CH_TXBUFF_TO_TXFIFO        0 /**< \brief Dma channel used for TFT Master Qspi Transmit  */
#define TFT_DMA_CH_RXBUFF_FROM_RXFIFO      1 /**< \brief Dma channel used for TFT Master Qspi Receive  */
#define SDCARD_DMA_CH_TXBUFF_TO_TXFIFO     2 /**< \brief Dma channel used for SDCARD Master Qspi Transmit  */
#define SDCARD_DMA_CH_RXBUFF_FROM_RXFIFO   3 /**< \brief Dma channel used for SDCARD Master Qspi Receive  */

/** \} */


/**
 * \name Interrupt service provider configuration (use only number for variable section definition).
 * \{ */
#define ISR_PROVIDER_ASCLIN0_TX     IfxSrc_Tos_cpu0 /**< \brief Define the ASCLIN1 transmit interrupt provider.  */
#define ISR_PROVIDER_ASCLIN0_RX     IfxSrc_Tos_cpu0 /**< \brief Define the ASCLIN1 receive interrupt provider.  */
#define ISR_PROVIDER_ASCLIN0_ER     IfxSrc_Tos_cpu0 /**< \brief Define the ASCLIN1 error interrupt provider.  */

#define ISR_PROVIDER_ASCLIN1_TX     IfxSrc_Tos_cpu0 /**< \brief Define the ASCLIN1 transmit interrupt provider.  */
#define ISR_PROVIDER_ASCLIN1_RX     IfxSrc_Tos_cpu0 /**< \brief Define the ASCLIN1 receive interrupt provider.  */
#define ISR_PROVIDER_ASCLIN1_ER     IfxSrc_Tos_cpu0 /**< \brief Define the ASCLIN1 error interrupt provider.  */

#define ISR_PROVIDER_QSPI0_TX       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi0 transmit interrupt provider.  */
#define ISR_PROVIDER_QSPI0_RX       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi0 receive interrupt provider.  */
#define ISR_PROVIDER_QSPI0_ER       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi0 error interrupt provider.  */

#define ISR_PROVIDER_QSPI1_TX       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi0 transmit interrupt provider.  */
#define ISR_PROVIDER_QSPI1_RX       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi0 receive interrupt provider.  */
#define ISR_PROVIDER_QSPI1_ER       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi0 error interrupt provider.  */

#define ISR_PROVIDER_QSPI2_TX       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi2 transmit interrupt provider.  */
#define ISR_PROVIDER_QSPI2_RX       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi2 receive interrupt provider.  */
#define ISR_PROVIDER_QSPI2_ER       IfxSrc_Tos_cpu0      /**< \brief Define the Qspi2 error interrupt provider.  */

#define ISR_PROVIDER_QSPI0          IfxSrc_Tos_cpu0     /**< \brief Define the Qspi2 interrupt provider.  */
#define ISR_PROVIDER_QSPI1          IfxSrc_Tos_cpu0     /**< \brief Define the Qspi2 interrupt provider.  */
#define ISR_PROVIDER_QSPI2          IfxSrc_Tos_cpu0     /**< \brief Define the Qspi2 interrupt provider.  */
#define ISR_PROVIDER_QSPI3          IfxSrc_Tos_cpu0     /**< \brief Define the Qspi2 interrupt provider.  */

#define ISR_PROVIDER_TIMER          IfxSrc_Tos_cpu0 /**< \brief Define the 1ms timer interrupt provider.  */

#define ISR_PROVIDER_CPUSRV0      	0//IfxSrc_Tos_cpu0  	/**< \brief Define the touch data receive interrupt provider.  */

#define ISR_CPUSRV0		      		5  /**< \brief Define the touch data receive interrupt provider.  */         // CPU

#define ISR_PROVIDER_MODULE_GTM_TOM0            IfxSrc_Tos_cpu0
#define ISR_PROVIDER_MODULE_GTM_TOM1            IfxSrc_Tos_cpu0

#define ISR_PROVIDER_CAN_TX         IfxSrc_Tos_cpu0                           /* Define the CAN TX interrupt provider              */
#define ISR_PROVIDER_CAN_RX         IfxSrc_Tos_cpu0                           /* Define the CAN RX interrupt provider              */

/** \} */
/**
 * \name Interrupt configuration.
 * \{ */
#define INTERRUPT_ASCLIN0_TX        ISR_ASSIGN(ISR_PRIORITY_ASCLIN0_TX, ISR_PROVIDER_ASCLIN0_TX) /**< \brief Define the ASC0 transmit interrupt priority used by printf.c */
#define INTERRUPT_ASCLIN0_EX        ISR_ASSIGN(ISR_PRIORITY_ASCLIN0_EX, ISR_PROVIDER_ASCLIN0_EX) /**< \brief Define the ASC0 transmit interrupt priority used by printf.c */
#define INTERRUPT_ASCLIN0_RX        ISR_ASSIGN(ISR_PRIORITY_ASCLIN0_RX, ISR_PROVIDER_ASCLIN0_RX) /**< \brief Define the ASC0 transmit interrupt priority used by printf.c */

#define INTERRUPT_ASCLIN1_TX        ISR_ASSIGN(ISR_PRIORITY_ASCLIN1_TX, ISR_PROVIDER_ASCLIN1_TX) /**< \brief Define the ASC0 transmit interrupt priority used by printf.c */
#define INTERRUPT_ASCLIN1_EX        ISR_ASSIGN(ISR_PRIORITY_ASCLIN1_EX, ISR_PROVIDER_ASCLIN1_EX) /**< \brief Define the ASC0 transmit interrupt priority used by printf.c */
#define INTERRUPT_ASCLIN1_RX        ISR_ASSIGN(ISR_PRIORITY_ASCLIN1_RX, ISR_PROVIDER_ASCLIN1_RX) /**< \brief Define the ASC0 transmit interrupt priority used by printf.c */

#define INTERRUPT_QSPI0_TX          ISR_ASSIGN(ISR_PRIORITY_QSPI0_TX, ISR_PROVIDER_QSPI0_TX)             /**< \brief Define the QSPI0 transmit interrupt priority.  */
#define INTERRUPT_QSPI0_RX          ISR_ASSIGN(ISR_PRIORITY_QSPI0_RX, ISR_PROVIDER_QSPI0_RX)             /**< \brief Define the QSPI0 receive interrupt priority.  */
#define INTERRUPT_QSPI0_ER          ISR_ASSIGN(ISR_PRIORITY_QSPI0_ER, ISR_PROVIDER_QSPI0_ER)             /**< \brief Define the QSPI0 error interrupt priority.  */

#define INTERRUPT_QSPI1_TX          ISR_ASSIGN(ISR_PRIORITY_QSPI1_TX, ISR_PROVIDER_QSPI1_TX)             /**< \brief Define the QSPI1 transmit interrupt priority.  */
#define INTERRUPT_QSPI1_RX          ISR_ASSIGN(ISR_PRIORITY_QSPI1_RX, ISR_PROVIDER_QSPI1_RX)             /**< \brief Define the QSPI1 receive interrupt priority.  */
#define INTERRUPT_QSPI1_ER          ISR_ASSIGN(ISR_PRIORITY_QSPI1_ER, ISR_PROVIDER_QSPI1_ER)             /**< \brief Define the QSPI1 error interrupt priority.  */

#define INTERRUPT_QSPI2_TX          ISR_ASSIGN(ISR_PRIORITY_QSPI2_TX, ISR_PROVIDER_QSPI2_TX)             /**< \brief Define the QSPI2 transmit interrupt priority.  */
#define INTERRUPT_QSPI2_RX          ISR_ASSIGN(ISR_PRIORITY_QSPI2_RX, ISR_PROVIDER_QSPI2_RX)             /**< \brief Define the QSPI2 receive interrupt priority.  */
#define INTERRUPT_QSPI2_ER          ISR_ASSIGN(ISR_PRIORITY_QSPI2_ER, ISR_PROVIDER_QSPI2_ER)             /**< \brief Define the QSPI2 error interrupt priority.  */



//#define INTERRUPT_TIMER             ISR_ASSIGN(ISR_PRIORITY_TIMER, ISR_PROVIDER_TIMER)                   /**< \brief Define the 1ms timer interrupt priority.  */

/** \} */

/** \} */
//------------------------------------------------------------------------------

#endif
