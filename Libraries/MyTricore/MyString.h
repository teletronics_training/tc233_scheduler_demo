/*
 * MyString.h
 *
 *  Created on: Jul 5, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_MYSTRING_H_
#define LIBRARIES_MYTRICORE_MYSTRING_H_

#include "Ifx_Types.h"


//Add define here


//Add ENUM here


//Add Struct here


//Add export variable here


//Add function prototype here

IFX_EXTERN void uint2hex(char *str,unsigned char data);
IFX_EXTERN void reverse(char* str, int len);
IFX_EXTERN int intToStr(int x, char str[], int d);
IFX_EXTERN void ftoa(float n, char* res, int afterpoint);

#endif /* LIBRARIES_MYTRICORE_MYSTRING_H_ */
