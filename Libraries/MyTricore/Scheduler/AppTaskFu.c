/*
 * AppTaskFu.c
 *
 *  Created on: 19.09.2013
 *
 */


#include "AppTaskFu.h"
#include "ConfigurationECU.h"
#include "LCD_SCREEN.h"
#include "MULTICANBUS.h"
#include "GTM_TIM_PWM.h"
#include "PWM_GPIO.h"
#include "Relay_GPIO.h"
#include "GPIO.h"
#include "POT_SPI.h"

typedef enum
{
    TEST_BOARD=0,
    TEST_RC,
    TEST_PWM,
    TEST_Relay,
    TEST_GPIO,
    TEST_Number
}Test_Senario_Type;

Test_Senario_Type senario=TEST_RC;


void appTaskfu_1ms(void)
{

}

void appTaskfu_10ms(void)
{

}

void appTaskfu_100ms(void)
{
    char string1[32],string2[32],string3[32],string4[32];
    sprintf(string1,"ECU Board TC233 ....");
    sprintf(string2,"Scheduler...........");
    sprintf(string3,"....................");
    sprintf(string4,"....................");
    LCD_print(string1);
    LCD_print(string2);
    LCD_print(string3);
    LCD_print(string4);
}

void appTaskfu_1000ms(void)
{
    LCD_draw();
}

