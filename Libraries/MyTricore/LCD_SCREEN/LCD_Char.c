/**
 * \file QspiTlfDemo.c
 * \brief
 *
 * \version V0.2
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "LCD_Char.h"
#include "Configuration.h"
#include "SysSe/Bsp/Bsp.h"
#include "MyBsp.h"

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/


/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/


/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/
#if LCD_Char_VAR_LOCATION == 0
	#if defined(__GNUC__)
	#pragma section ".bss_cpu0" awc0
	#endif
	#if defined(__TASKING__)
	#pragma section farbss "bss_cpu0"
	#pragma section fardata "data_cpu0"
	#endif
	#if defined(__DCC__)
	#pragma section DATA ".data_cpu0" ".bss_cpu0" far-absolute RW
	#endif
#elif LCD_Char_VAR_LOCATION == 1
	#if defined(__GNUC__)
	#pragma section ".bss_cpu1" awc1
	#endif
	#if defined(__TASKING__)
	#pragma section farbss "bss_cpu1"
	#pragma section fardata "data_cpu1"
	#endif
	#if defined(__DCC__)
	#pragma section DATA ".data_cpu1" ".bss_cpu1" far-absolute RW
	#endif
#elif LCD_Char_VAR_LOCATION == 2
	#if defined(__GNUC__)
	#pragma section ".bss_cpu2" awc2
	#endif
	#if defined(__TASKING__)
	#pragma section farbss "bss_cpu2"
	#pragma section fardata "data_cpu2"
	#endif
	#if defined(__DCC__)
	#pragma section DATA ".data_cpu2" ".bss_cpu2" far-absolute RW
	#endif
#else
#error "Set LCD_Char_VAR_LOCATION to a valid value!"
#endif

App_Qspi_LCD_Char_Cpu g_Qspi_LCD_Char_Cpu;
boolean LCD_Char_A_Step;   // this is the value of DEVCTRL Register which should be different between A and other steps

#if defined(__GNUC__)
#pragma section
#endif
#if defined(__TASKING__)
#pragma section farbss restore
#pragma section fardata restore
#endif
#if defined(__DCC__)
#pragma section DATA RW
#endif

/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/


#if defined(AurixBoard_TC265)
IFX_INTERRUPT(qspi1TxISR, 0, ISR_PRIORITY_QSPI1_TX);
IFX_INTERRUPT(qspi1RxISR, 0, ISR_PRIORITY_QSPI1_RX);
IFX_INTERRUPT(qspi1ErISR, 0, ISR_PRIORITY_QSPI1_ER);
#elif defined(AurixBoard_TC233)
IFX_INTERRUPT(qspi2TxISR, 0, ISR_PRIORITY_QSPI2_TX);
IFX_INTERRUPT(qspi2RxISR, 0, ISR_PRIORITY_QSPI2_RX);
IFX_INTERRUPT(qspi2ErISR, 0, ISR_PRIORITY_QSPI2_ER);
#else
#error AurixBoard selection required.
#endif



/******************************************************************************/
/*------------------------Private Variables/Constants-------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/
/** \brief TLF (QSPI) initialization
 *
 * This function initializes Qspix in master mode.
 */
void LCD_Char_init(void)
{
    /* disable interrupts */
    boolean interruptState = IfxCpu_disableInterrupts();
    // install interrupt handlers
//    IfxCpu_Irq_installInterruptHandler(&qspi1TxISR, ISR_PRIORITY_QSPI1_TX);
//    IfxCpu_Irq_installInterruptHandler(&qspi1RxISR, ISR_PRIORITY_QSPI1_RX);
//    IfxCpu_Irq_installInterruptHandler(&qspi1ErISR, ISR_PRIORITY_QSPI1_ER);
//    IfxCpu_enableInterrupts();

    // create module config
    IfxQspi_SpiMaster_Config spiMasterConfig;
#if defined(AurixBoard_TC265)
    IfxQspi_SpiMaster_initModuleConfig(&spiMasterConfig, &MODULE_QSPI1);
#elif defined(AurixBoard_TC233)
    IfxQspi_SpiMaster_initModuleConfig(&spiMasterConfig, &MODULE_QSPI2);
#else
#error AurixBoard selection required.
#endif


    // set the desired mode and maximum baudrate
    spiMasterConfig.base.mode             = SpiIf_Mode_master;
    spiMasterConfig.base.maximumBaudrate  = 5000000;

    // ISR priorities and interrupt target

#if defined(AurixBoard_TC265)
    spiMasterConfig.base.txPriority       = ISR_PRIORITY_QSPI1_TX;
    spiMasterConfig.base.rxPriority       = ISR_PRIORITY_QSPI1_RX;
    spiMasterConfig.base.erPriority       = ISR_PRIORITY_QSPI1_ER;
    spiMasterConfig.base.isrProvider      = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());
#elif defined(AurixBoard_TC233)
    spiMasterConfig.base.txPriority       = ISR_PRIORITY_QSPI2_TX;
    spiMasterConfig.base.rxPriority       = ISR_PRIORITY_QSPI2_RX;
    spiMasterConfig.base.erPriority       = ISR_PRIORITY_QSPI2_ER;
    spiMasterConfig.base.isrProvider      = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());
#else
#error AurixBoard selection required.
#endif

    // pin configuration
#if defined(AurixBoard_TC265)
    const IfxQspi_SpiMaster_Pins pins = {
        &QSPI1_SCLK_PIN, IfxPort_OutputMode_pushPull, // SCLK
        &QSPI1_MTSR_PIN, IfxPort_OutputMode_pushPull, // MTSR
        &QSPI1_MRST_PIN, IfxPort_InputMode_pullDown,  // MRST
        IfxPort_PadDriver_cmosAutomotiveSpeed3 // pad driver mode
    };
#elif defined(AurixBoard_TC233)
    const IfxQspi_SpiMaster_Pins pins = {
        &QSPI2_SCLK_PIN, IfxPort_OutputMode_pushPull, // SCLK
        &QSPI2_MTSR_PIN, IfxPort_OutputMode_pushPull, // MTSR
        &QSPI2_MRST_PIN, IfxPort_InputMode_pullDown,  // MRST
        IfxPort_PadDriver_cmosAutomotiveSpeed3 // pad driver mode
    };
#else
#error AurixBoard selection required.
#endif


    spiMasterConfig.pins = &pins;

    // initialize module
    //IfxQspi_SpiMaster spi; // defined globally
    IfxQspi_SpiMaster_initModule(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster, &spiMasterConfig);

    // create channel config
    IfxQspi_SpiMaster_ChannelConfig spiMasterChannelConfig;
    IfxQspi_SpiMaster_initChannelConfig(&spiMasterChannelConfig, &g_Qspi_LCD_Char_Cpu.drivers.spiMaster);

    // set the baudrate for this channel
    spiMasterChannelConfig.base.baudrate = 10000;
    spiMasterChannelConfig.base.mode.csActiveLevel = Ifx_ActiveState_high;
    spiMasterChannelConfig.base.mode.csTrailDelay = 2;
    spiMasterChannelConfig.base.mode.csInactiveDelay = 2;
    spiMasterChannelConfig.base.mode.shiftClock = SpiIf_ShiftClock_shiftTransmitDataOnTrailingEdge;

    // select pin configuration
    const IfxQspi_SpiMaster_Output slsOutput = {
        &LCD_ENABLE,
        IfxPort_OutputMode_pushPull,
        IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    spiMasterChannelConfig.sls.output = slsOutput;

    // initialize channel
    //IfxQspi_SpiMaster_Channel spiChannel; // defined globally
    IfxQspi_SpiMaster_initChannel(&g_Qspi_LCD_Char_Cpu.drivers.spiMasterChannel, &spiMasterChannelConfig);

    /* init tx buffer area */
    g_Qspi_LCD_Char_Cpu.qspiBuffer.spiTxBuffer[0] = 0;
    g_Qspi_LCD_Char_Cpu.qspiBuffer.spiRxBuffer[0] = 0;
//
//    /* enable interrupts again */
    IfxCpu_restoreInterrupts(interruptState);
    IfxPort_setPinMode(LCD_STROBE, IfxPort_Mode_outputPushPullGeneral);
    initTime();
//
	Ifx_TickTime time;
    time = getDeadLine(TimeConst_1ms);

	time = getDeadLine(TimeConst_1ms*15);
    while(isDeadLine(time) != TRUE);

	LCD_Char_write(0x00);
	LCD_Char_write(0x30);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_1us*37);
    while(isDeadLine(time) != TRUE);

	LCD_Char_write(0x00);
	LCD_Char_write(0x38);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_1us*37);
    while(isDeadLine(time) != TRUE);

	LCD_Char_write(0x00);
	LCD_Char_write(0x08);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_1us*37);
    while(isDeadLine(time) != TRUE);

	LCD_Char_write(0x00);
	LCD_Char_write(0x01);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_10us*160);
    while(isDeadLine(time) != TRUE);

	LCD_Char_write(0x00);
	LCD_Char_write(0x06);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_1us*37);
    while(isDeadLine(time) != TRUE);

	LCD_Char_write(0x00);
	LCD_Char_write(0x0E);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_1us*37);
    while(isDeadLine(time) != TRUE);

}

void LCD_Char_strobe(void)
{
	Ifx_TickTime time;
    time = getDeadLine(TimeConst_1us);

	IfxPort_setPinState (LCD_STROBE, IfxPort_State_high);
    while(isDeadLine(time) != TRUE);
	IfxPort_setPinState (LCD_STROBE, IfxPort_State_low);
}

uint32 LCD_Char_write(uint32 send_data)
{
	g_Qspi_LCD_Char_Cpu.qspiBuffer.spiTxBuffer[0] = send_data;

    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_LCD_Char_Cpu.drivers.spiMasterChannel) == SpiIf_Status_busy)  {};

    IfxQspi_SpiMaster_exchange(&g_Qspi_LCD_Char_Cpu.drivers.spiMasterChannel, &g_Qspi_LCD_Char_Cpu.qspiBuffer.spiTxBuffer[0],
        &g_Qspi_LCD_Char_Cpu.qspiBuffer.spiRxBuffer[0], TLF_BUFFER_SIZE);

    /* we wait until our values are read from Qspi */
    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_LCD_Char_Cpu.drivers.spiMasterChannel) == SpiIf_Status_busy)  {};

    return (g_Qspi_LCD_Char_Cpu.qspiBuffer.spiRxBuffer[0]);

}

//sends a char to the LCD
void char2lcd(char a_char)
{
	Ifx_TickTime time;
	LCD_Char_write(0x01);
	LCD_Char_write(a_char);
	LCD_Char_strobe();
	time = getDeadLine(TimeConst_1us*37);
    while(isDeadLine(time) != TRUE);
}

//sends a string in FLASH to LCD
void string2lcd(char *lcd_str){
	int count;
	for (count=0; count<=(strlen(lcd_str)-1); count++){
		char2lcd(lcd_str[count]);
	}
}

void clear_display(void){
	Ifx_TickTime time;
	LCD_Char_write(0x00);
	LCD_Char_write(0x01);
	LCD_Char_strobe();   //strobe the LCD enable pin
	time = getDeadLine(TimeConst_10us*160);
    while(isDeadLine(time) != TRUE);
}

void home_line1(void){
	Ifx_TickTime time;
	LCD_Char_write(0x00);
	LCD_Char_write(0x80);
	LCD_Char_strobe();   //strobe the LCD enable pin
	time = getDeadLine(TimeConst_10us*160);
    while(isDeadLine(time) != TRUE);
}

void home_line2(void){
	Ifx_TickTime time;
	LCD_Char_write(0x00);
	LCD_Char_write(0xC0);
	LCD_Char_strobe();   //strobe the LCD enable pin
	time = getDeadLine(TimeConst_10us*160);
    while(isDeadLine(time) != TRUE);
}

void home_line3(void){
	Ifx_TickTime time;
	LCD_Char_write(0x00);
	LCD_Char_write(0x94);
	LCD_Char_strobe();   //strobe the LCD enable pin
	time = getDeadLine(TimeConst_10us*160);
    while(isDeadLine(time) != TRUE);
}

void home_line4(void){
	Ifx_TickTime time;
	LCD_Char_write(0x00);
	LCD_Char_write(0xD4);
	LCD_Char_strobe();   //strobe the LCD enable pin
	time = getDeadLine(TimeConst_10us*160);
    while(isDeadLine(time) != TRUE);
}

char *char2hex(char *str,uint8 *hex,int len)
{
	char temp[128];
	int i;
	str[0]='\0';
	for(i=0;i<len;i++)
	{
		sprintf(temp,"%s%02x",str,hex[i]);
		sprintf(str,"%s",temp);
	}
}


#if defined(AurixBoard_TC265)

IFX_INTERRUPT(qspi1TxISR, 0, ISR_PRIORITY_QSPI1_TX)
{
    IfxQspi_SpiMaster_isrTransmit(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi1RxISR, 0, ISR_PRIORITY_QSPI1_RX)
{
    IfxQspi_SpiMaster_isrReceive(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi1ErISR, 0, ISR_PRIORITY_QSPI1_ER)
{
    IfxQspi_SpiMaster_isrError(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);

    // Process errors. Eg: parity Error is checked below
    //IfxQspi_SpiMaster_Channel *chHandle   = IfxQspi_SpiMaster_activeChannel(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);
    //if( chHandle->errorFlags.parityError == 1)
    {
        // Parity Error
    }
}

#elif defined(AurixBoard_TC233)

IFX_INTERRUPT(qspi2TxISR, 0, ISR_PRIORITY_QSPI2_TX)
{
    IfxQspi_SpiMaster_isrTransmit(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi2RxISR, 0, ISR_PRIORITY_QSPI2_RX)
{
    IfxQspi_SpiMaster_isrReceive(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi2ErISR, 0, ISR_PRIORITY_QSPI2_ER)
{
    IfxQspi_SpiMaster_isrError(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);

    // Process errors. Eg: parity Error is checked below
    //IfxQspi_SpiMaster_Channel *chHandle   = IfxQspi_SpiMaster_activeChannel(&g_Qspi_LCD_Char_Cpu.drivers.spiMaster);
    //if( chHandle->errorFlags.parityError == 1)
    {
        // Parity Error
    }
}

#else
#error AurixBoard selection required.
#endif



