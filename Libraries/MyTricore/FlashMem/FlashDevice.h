/*
 * FlashDevice.h
 *
 *  Created on: Sep 24, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_FLASHDEVICE_H_
#define LIBRARIES_MYTRICORE_FLASHDEVICE_H_

#include <Cpu/Std/Ifx_Types.h>

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here
/* fields in SPI flash status register */
#define SPIFLASH_BSY_BIT        0x00000001 /* WIP Bit of SPI SR on SMI SR */
#define SPIFLASH_WE_BIT         0x00000002 /* WEL Bit of SPI SR on SMI SR */

/* SPI Flash Commands */
#define SPIFLASH_READ_ID        0x9F /* Read Flash Identification */
#define SPIFLASH_READ_STATUS    0x05 /* Read Status Register */
#define SPIFLASH_WRITE_ENABLE   0x06 /* Write Enable */
#define SPIFLASH_PAGE_PROGRAM   0x02 /* Page Program */
#define SPIFLASH_FAST_READ      0x0B /* Fast Read */
#define SPIFLASH_READ           0x03 /* Normal Read */

#define FLASH_ID(n, es, ces, id, psize, ssize, size) \
{                           \
    .name = n,              \
    .erase_cmd = es,        \
    .chip_erase_cmd = ces,  \
    .device_id = id,        \
    .pagesize = psize,      \
    .sectorsize = ssize,    \
    .size_in_bytes = size   \
}

//Add ENUM here


//Add Struct here
struct flash_device {
    char *name;
    uint8 erase_cmd;
    uint8 chip_erase_cmd;
    uint32 device_id;
    uint32 pagesize;
    unsigned long sectorsize;
    unsigned long size_in_bytes;
};

//Add export variable here
IFX_EXTERN const struct flash_device flash_devices[];

//Add function prototype here


#endif /* LIBRARIES_MYTRICORE_FLASHDEVICE_H_ */
