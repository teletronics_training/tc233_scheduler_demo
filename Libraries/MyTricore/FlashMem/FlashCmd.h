/*
 * FlashCmd.h
 *
 *  Created on: Sep 24, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_FLASHMEM_FLASHCMD_H_
#define LIBRARIES_MYTRICORE_FLASHMEM_FLASHCMD_H_

#include <Cpu/Std/Ifx_Types.h>


/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here
#define FLASHMEM_FLAG_32BIT_ADDR     0x01    // larger than 16 MByte address
#define FLASHMEM_FLAG_STATUS_CMD70   0x02    // requires special busy flag check
#define FLASHMEM_FLAG_DIFF_SUSPEND   0x04    // uses 2 different suspend commands
#define FLASHMEM_FLAG_MULTI_DIE      0x08    // multiple die, don't read cross 32M barrier
#define FLASHMEM_FLAG_256K_BLOCKS    0x10    // has 256K erase blocks
#define FLASHMEM_FLAG_DIE_MASK       0xC0    // top 2 bits count during multi-die erase

//Add ENUM here


//Add Struct here

typedef struct
{
    uint16 dirindex; // current position for readdir()
    uint8 flags;   // chip features
    uint8 busy;    // 0 = ready
                // 1 = suspendable program operation
                // 2 = suspendable erase operation
                // 3 = busy for realz!!
}SerialFlashChip;

typedef struct
{
    uint32 address;  // where this file's data begins in the Flash, or zero
    uint32 length;   // total length of the data in the Flash chip
    uint32 offset; // current read/write offset in the file
    uint16 dirindex;
    SerialFlashChip SerialFlash;
}SerialFlashFile;

//Add export variable here

IFX_EXTERN SerialFlashFile g_SerialFlashFile;

//Add function prototype here

IFX_EXTERN uint32 SerialFlashInit();
IFX_EXTERN uint32 SerialFlashCapacity(const uint8 *id);
IFX_EXTERN uint32 SerialFlashBlockSize();
IFX_EXTERN void SerialFlashSleep();
IFX_EXTERN void SerialFlashWakeup();
IFX_EXTERN void SerialFlashReadID(uint8 *buf);
IFX_EXTERN void SerialFlashReadSerialNumber(uint8 *buf);
IFX_EXTERN void SerialFlashRead(uint32 addr, void *buf, uint32 len);
//IFX_EXTERN bool SerialFlashReady();
//IFX_EXTERN void SerialFlashWait();
IFX_EXTERN void SerialFlashWrite(uint32 addr, const void *buf, uint32 len);
IFX_EXTERN void SerialFlashEraseAll();
IFX_EXTERN void SerialFlashEraseBlock(uint32 addr);

IFX_EXTERN SerialFlashFile SerialFlashOpen(const char *filename);
//IFX_EXTERN bool SerialFlashCreate(const char *filename, uint32 length, uint32 align = 0);
//IFX_EXTERN bool SerialFlashCreateErasable(const char *filename, uint32 length) {
//    return create(filename, length, blockSize());
//}
//IFX_EXTERN bool SerialFlashExists(const char *filename);
//IFX_EXTERN bool SerialFlashRemove(const char *filename);
//IFX_EXTERN bool SerialFlashRemove(SerialFlashFile &file);
//IFX_EXTERN void SerialFlashOpendir() { dirindex = 0; }
//IFX_EXTERN bool SerialFlashReaddir(char *filename, uint32 strsize, uint32 &filesize);


#endif /* LIBRARIES_MYTRICORE_FLASHMEM_FLASHCMD_H_ */
