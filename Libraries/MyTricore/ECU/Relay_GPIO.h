/*
 * Relay_GPIO.h
 *
 *  Created on: Oct 2, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_ECU_RELAY_GPIO_H_
#define LIBRARIES_MYTRICORE_ECU_RELAY_GPIO_H_

#include <Cpu/Std/Ifx_Types.h>
#include "ConfigurationECU.h"
#include "Port/Std/IfxPort.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else

#error AurixBoard selection required.
#endif
*/

//Add define here


//Add ENUM here


typedef enum
{
    Relay_GPIO_0 = 0,
    Relay_GPIO_1,
    Relay_GPIO_2,
    Relay_GPIO_3,
    Relay_GPIO_4,
    Relay_GPIO_5,
    Relay_GPIO_6,
    Relay_GPIO_7,
    Relay_GPIO_Num
} App_Relay_GPIO_Port;

//Add Struct here

typedef struct
{
    Ifx_P *port;
    uint8 pinIndex;
    IfxPort_State activeState;
    boolean status;
}App_Relay_GPIO_OutPin;

//Add export variable here

IFX_EXTERN App_Relay_GPIO_OutPin g_App_Relay_GPIO_OutPin[Relay_GPIO_Num];

//Add function prototype here

IFX_EXTERN void Relay_GPIO_init(void);
IFX_EXTERN void Relay_GPIO_on(App_Relay_GPIO_Port gpio);
IFX_EXTERN void Relay_GPIO_off(App_Relay_GPIO_Port gpio);

#endif /* LIBRARIES_MYTRICORE_ECU_RELAY_GPIO_H_ */

