/*
 * AppTimDriver.c
 *
 *  Created on: Oct 8, 2020
 *      Author: weeti
 */

#include <GTM_TIM_PWM.h>
#include "IfxGtm_PinMap.h"

#define GTM_TIM_Channel 6

App_GtmTimPwm g_GtmTimPwm[6]=
        {
                {
                        .pwmIn=&IfxGtm_TIM0_0_TIN0_P02_0_IN
                },
                {
                        .pwmIn=&IfxGtm_TIM0_1_TIN1_P02_1_IN
                },
                {
                        .pwmIn=&IfxGtm_TIM0_2_TIN2_P02_2_IN
                },
                {
                        .pwmIn=&IfxGtm_TIM0_6_TIN6_P02_6_IN
                },
                {
                        .pwmIn=&IfxGtm_TIM0_7_TIN7_P02_7_IN
                },
                {
                        .pwmIn=&IfxGtm_TIM0_3_TIN3_P02_3_IN
                }
        };

void initGtmTimPwm(void)
{
	int i;
    IfxGtm_enable(&MODULE_GTM);                                         /* Enable the GTM                           */
    IfxGtm_Cmu_enableClocks(&MODULE_GTM, IFXGTM_CMU_CLKEN_CLK0);        /* Enable the CMU clock 0                   */
	for(i=0;i<GTM_TIM_Channel;i++)
	{
	    IfxGtm_Tim_In_Config config;
	    IfxGtm_Tim_In_initConfig(&config, &MODULE_GTM);
	    config.filter.inputPin=g_GtmTimPwm[i].pwmIn;
	    config.filter.inputPinMode = IfxPort_InputMode_pullDown;
	    IfxGtm_Tim_In_init(&g_GtmTimPwm[i].Tim, &config);
	}
}

void getDutyCycle(void)
{
    int i;
    for(i=0;i<GTM_TIM_Channel;i++)
    {
        IfxGtm_Tim_In_update(&g_GtmTimPwm[i].Tim);
        g_GtmTimPwm[i].g_measuredPwmPeriod = IfxGtm_Tim_In_getPeriodSecond(&g_GtmTimPwm[i].Tim);          /* Get the period of the PWM signal */
        g_GtmTimPwm[i].g_measuredPwmFreq_Hz = 1 / g_GtmTimPwm[i].g_measuredPwmPeriod;                             /* Calculate the frequency          */
        g_GtmTimPwm[i].g_measuredPwmDutyCycle = IfxGtm_Tim_In_getDutyPercent(&g_GtmTimPwm[i].Tim, &g_GtmTimPwm[i].g_dataCoherent); /* Get the duty cycle     */
    }
}
